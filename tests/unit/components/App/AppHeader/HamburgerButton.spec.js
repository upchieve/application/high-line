import { shallowMount } from '@vue/test-utils'
import { merge } from 'lodash-es'
import { createStore } from 'vuex'
import { storeOptions } from '@/store'
import HamburgerButton from '@/components/App/AppHeader/HamburgerButton.vue'
import { vi } from 'vitest'

const expand = vi.fn()
const collapse = vi.fn()

const getWrapper = (isCollapsed = true) => {
  const store = createStore(
    merge({}, storeOptions, {
      modules: {
        app: {
          modules: {
            sidebar: {
              state: { isCollapsed },
              actions: { expand, collapse },
            },
          },
        },
      },
    })
  )

  return shallowMount(HamburgerButton, {
    global: { plugins: [store] },
  })
}

describe.skip('HamburgerButton', () => {
  it('renders expected elements', () => {
    const wrapper = getWrapper(true)
    expect(wrapper.classes()).toEqual(['icon'])
  })

  it("renders 'hamburger' if sidebar is collapsed", () => {
    // const wrapper = getWrapper(true);
    // expect(wrapper.vm.icon).toBe(HamburgerIcon);
  })

  it("renders 'cross' if sidebar is expanded", () => {
    // const wrapper = getWrapper(false);
    // expect(wrapper.vm.icon).toBe(CrossIcon);
  })

  // The tests below are failing to trigger the handleClick function
  it('calls handleClick when clicked', () => {
    // const handleClick = vi.fn();
    // const wrapper = getWrapper(true);
    // const icon = wrapper.find(HamburgerIcon);
    // icon.trigger("click");
    /**
     *
     * TODO: Fix test - remove explicit call to handleClick function
     * and have the click trigger call the handleClick function
     *
     **/
    // handleClick();
    // expect(handleClick).toHaveBeenCalled();
  })

  it('expands sidebar if collapsed when clicked', () => {
    // const wrapper = getWrapper(true);
    // // const icon = wrapper.find(HamburgerIcon);
    // icon.trigger("click");
    /**
     *
     * TODO: Fix test - remove explicit call to expand function and
     * have the click trigger call the expand function
     *
     **/
    // expand();
    // expect(expand).toHaveBeenCalled();
  })

  it('collapses sidebar if expanded when clicked', () => {
    // const wrapper = getWrapper(false);
    // const icon = wrapper.find(CrossIcon);
    /**
     *
     * TODO: Fix test - remove explicit call to collapse function and
     * have the click trigger call the collapse function
     *
     **/
    // icon.trigger("click");
    // collapse();
    // expect(collapse).toHaveBeenCalled();
  })
})
