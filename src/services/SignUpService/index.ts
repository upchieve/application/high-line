import { EVENTS } from '@/consts'
import AnalyticsService from '@/services/AnalyticsService'
import LoggerService from '@/services/LoggerService'
import NetworkService from '@/services/NetworkService'
import { Provider, signInWithSso } from '@/services/SsoService'

export enum SignUpPage {
  account = 'account',
  eligibility = 'eligibility',
  ineligible = 'ineligible',
  parentGuardianConfirmation = 'confirmation',
  partnerInfo = 'info',
  verify = 'verify',
}

export enum UserType {
  student = 'student',
  teacher = 'teacher',
  volunteer = 'volunteer',
}

export type PageDetail = {
  backgroundLayout?:
    | 'full'
    | 'card'
    | 'panel-left-50p'
    | 'panel-left-75p'
    | 'panel-right-50p'
    | 'panel-right-75p'
  panelImage?: string
  submitAction: SubmitAction
  classes?: string
  rows: FormRow[]
}

type SubmitAction = (
  data: Object
) => Promise<SubmitActionResponse> | SubmitActionResponse
export type SubmitActionResponse = [
  {
    params?: { step: string; userType?: UserType }
    path?: string
    query?: any
  } | null,
  ErrorMessage | null,
]
type ErrorMessage = string
export type FormRow = {
  classes: string
  elements: FormElement[]
}
export type FormElement = {
  element: FormElementType
  classes?: string
  content?: string
  isDisabledOnInvalid?: boolean
  submitAction?: SubmitAction
  props?: any // TODO: Make generic for each FormElement.
  showIf?: (form: any) => boolean
}
type FormElementType =
  | 'h1'
  | 'h2'
  | 'p'
  | 'a'
  | 'button'
  | 'FormCheckBox'
  | 'FormSelect'
  | 'FormInput'
  | 'FormEmail'
  | 'FormSchoolSearch'
  | 'FormPassword'
  | 'SsoButton'
  | 'LineDivider'
  | 'router-link'
  | 'check-circled'
  | 'header-logo-teal'
  | 'updog-crying'
  | 'updog-smiling'

export function getFilteredPageDetails(cb: () => PageDetail): PageDetail {
  const pd = cb()
  pd.rows = pd.rows.filter((row) => !!row)
  return pd
}

export function getSubmitResponseDefault(
  nextPage: string | null,
  data: Object | null,
  err?: any
): SubmitActionResponse | undefined {
  if (err) {
    const error =
      typeof err === 'string'
        ? err
        : err.response?.data?.err ?? 'Failed: Please try again.'
    return [null, error]
  }

  switch (nextPage) {
    case SignUpPage.account:
      return [
        {
          params: {
            ...data,
            step: SignUpPage.account,
          },
        },
        null,
      ]
    case SignUpPage.ineligible:
      return [
        {
          params: {
            ...data,
            step: SignUpPage.ineligible,
          },
        },
        null,
      ]
    case SignUpPage.verify:
      return [{ path: '/verify' }, null]
  }
}

export function continueToAccountPage(data: Object) {
  return getSubmitResponseDefault(SignUpPage.account, data)
}

export function createAccountWithGoogle(data) {
  AnalyticsService.captureEvent(EVENTS.USER_CLICKED_SIGN_UP_WITH_GOOGLE)
  return createAccountWithSso(Provider.GOOGLE, data)
}

export function createAccountWithClever(data) {
  AnalyticsService.captureEvent(EVENTS.USER_CLICKED_SIGN_UP_WITH_CLEVER)
  return createAccountWithSso(Provider.CLEVER, data)
}

function createAccountWithSso(
  provider: 'google' | 'clever',
  data: any
): SubmitActionResponse {
  try {
    signInWithSso({ provider, ...data })
    return [null, null]
  } catch (err) {
    LoggerService.noticeError(err)
    return getSubmitResponseDefault(null, null, err)
  }
}

export function getRow(classes = '', ...elements: FormElement[]): FormRow {
  return {
    classes,
    elements: elements.filter((e) => !!e),
  }
}

export function getTextElement(
  element: FormElementType,
  content: string
): FormElement {
  return {
    element,
    content,
  }
}

export function getInputElement(
  name: string,
  prettyName: string,
  blurEvent: string,
  classes
): FormElement {
  return {
    element: 'FormInput',
    classes,
    props: {
      blurEvent,
      name,
      label: prettyName,
      placeholder: prettyName,
    },
  }
}

export function getSsoButton(
  submitAction: SubmitAction,
  content: string,
  ssoMethod = 'google'
): FormElement {
  return {
    element: 'SsoButton',
    submitAction,
    props: {
      buttonText: content,
      ssoMethod,
    },
  }
}

export function getButtonElement(
  submitAction: SubmitAction,
  content: string,
  classes = '',
  isDisabledOnInvalid = true
): FormElement {
  return {
    element: 'button',
    classes: 'uc-form-button ' + classes,
    content,
    isDisabledOnInvalid,
    submitAction,
  }
}

export function getRouterLinkElement(
  content: string,
  pathTo: string
): FormElement {
  return {
    element: 'router-link',
    classes: 'uc-link',
    content: content,
    props: {
      to: pathTo,
    },
  }
}

export function getLinkElement(content: string, link: string): FormElement {
  return {
    element: 'a',
    classes: 'uc-link',
    content: content,
    props: {
      href: link,
    },
  }
}

export function getSignUpSourceElement(
  name: string,
  blurEvent: string
): FormElement {
  return {
    element: 'FormSelect',
    props: {
      blurEvent,
      getSelectOptions: async function () {
        try {
          const {
            data: { signupSources },
          } = await NetworkService.getStudentSignupSources()
          return signupSources
        } catch (err) {
          LoggerService.noticeError(err)
          return []
        }
      },
      label: 'How did you hear about us?',
      name,
      optionTextField: 'name',
      placeholder: 'How did you hear about us?',
      reduce: (option) => option.id,
    },
  }
}

export function getAlreadyHaveAccountElements() {
  return [
    getTextElement('p', 'Already have an account?'),
    getRouterLinkElement('Log in', '/login'),
  ]
}
