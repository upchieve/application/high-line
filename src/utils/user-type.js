export function isVolunteerUserType(userType) {
  return userType === 'volunteer'
}

export function isStudentUserType(userType) {
  return userType === 'student'
}

export function isTeacherUserType(userType) {
  return userType === 'teacher'
}
